import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { TranslateModule } from '@ngx-translate/core';
import { LocationStrategy, HashLocationStrategy, registerLocaleData } from '@angular/common';
import { MostModule } from '@themost/angular';
import { ConfigurationService, UserService } from '@universis/common';
import { SharedModule, ErrorModule, AuthModule } from '@universis/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppSidebarModule } from '@coreui/angular';
import { APP_LOCATIONS } from '@universis/common';
// Routing Module
import { AppRoutingModule } from './app.routing';
import { BreadcrumbsComponent } from './layouts/breadcrumbs.component';
import { ProfileService } from './profile/services/profile.service';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import * as locations from './app.locations';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { StudentsSharedModule } from './students-shared/students-shared.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RegistrationSharedModule } from './registrations/registration-shared.module';
import { RequestsSharedModule } from './requests/requests-shared.module';
import { MessageSharedService } from './students-shared/services/messages.service';
import { RequestsService } from './requests/services/requests.service';
import { DiningSharedModule } from '@universis/ngx-dining/shared';
import { NgxProfilesModule, NgxProfilesService, NgxProfilesUserService } from '@universis/ngx-profiles';

@NgModule({
    declarations: [
        AppComponent,
        FullLayoutComponent,
        BreadcrumbsComponent
    ],
    imports: [
        BrowserModule,
        ChartsModule,
        HttpClientModule,
        TranslateModule.forRoot(),
        MostModule.forRoot({
            base: '/',
            options: {
                useMediaTypeExtensions: false,
                useResponseConversion: true
            }
        }),
        SharedModule.forRoot(),
        StudentsSharedModule.forRoot(),
        RouterModule,
        AuthModule.forRoot(),
        FormsModule,
        AppRoutingModule,
        ErrorModule.forRoot(),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        BrowserAnimationsModule,
        ToastrModule.forRoot(
            {
                timeOut: 10000,
                extendedTimeOut: 5000,
                positionClass: 'toast-top-full-width',
                preventDuplicates: true,
                progressBar: true
            }
        ),
        TooltipModule.forRoot(),
        AppSidebarModule,
        RegistrationSharedModule.forRoot(),
        RequestsSharedModule.forRoot(),
        DiningSharedModule.forRoot(),
        NgxProfilesModule.forRoot()
    ],
    providers: [
        Title,
        {
            provide: APP_LOCATIONS, useValue: locations.STUDENTS_APP_LOCATIONS
        },
        {
            provide: APP_INITIALIZER,
            // use APP_INITIALIZER to load application configuration
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                    // load application configuration
                    return configurationService.load().then(loaded => {
                        // load angular locales
                        const sources = configurationService.settings.i18n.locales.map(locale => {
                            return import(`@angular/common/locales/${locale}.js`).then(module => {
                                // register locale data
                                registerLocaleData(module.default);
                                // return
                                return Promise.resolve();
                            });
                        });
                        return Promise.all(sources).then(() => {
                            // return true for APP_INITIALIZER
                            return Promise.resolve(true);
                        });
                    });
                },
            deps: [ConfigurationService],
            multi: true
        },
        {
            provide: LOCALE_ID,
            useFactory: (configurationService: ConfigurationService) => {
                return configurationService.currentLocale;
            },
            deps: [ConfigurationService]
        },
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        },
        ProfileService,
        RequestsService,
        MessageSharedService,
        {
            provide: UserService,
            useClass: NgxProfilesUserService
        }
    ],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {

}
