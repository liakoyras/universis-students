import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {GradesRoutingModule} from './grades-routing.module';
import {environment} from '../../environments/environment';
import {GradesRecentComponent} from './components/grades-recent/grades-recent.component';
import {GradesAllComponent} from './components/grades-all/grades-all.component';
import {GradesHomeComponent} from './components/grades-home/grades-home.component';
import {GradesStatboxComponent} from './components/grades-statbox/grades-statbox.component';
import {CollapseModule, ModalModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';
import {NgPipesModule} from 'ngx-pipes';
import {GradesSharedModule} from './grades-shared.module';
import {LocalizedAttributesPipe, ModalService, SharedModule} from '@universis/common';
import {TooltipModule} from 'ngx-bootstrap';
import {StudentsSharedModule} from '../students-shared/students-shared.module';
import { GradesThesesComponent } from './components/grades-theses/grades-theses.component';
import { ElementsModule } from '../elements/elements.module';
import {ChartsModule} from 'ng2-charts';
import {GradesStatboxBottomComponent} from './components/grades-statbox-bottom/grades-statbox-bottom.component';
import {FilterByPipe} from 'ngx-pipes';
import {RequestsSharedModule} from '../requests/requests-shared.module';
import {GradesErrorComponent} from './components/grades-error/grades-error.component';
import {GradeDistributionModalComponent} from './components/grade-distribution-modal/grade-distribution-modal.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    NgPipesModule,
    GradesRoutingModule,
    TranslateModule,
    SharedModule,
    CollapseModule,
    GradesSharedModule,
    TooltipModule,
    StudentsSharedModule,
    ElementsModule,
    ChartsModule,
    ModalModule.forRoot(),
    RequestsSharedModule
  ],
    declarations: [
        GradesRecentComponent,
        GradesAllComponent,
        GradesHomeComponent,
        GradesStatboxComponent,
        GradesStatboxBottomComponent,
        GradesThesesComponent,
        GradesErrorComponent,
        GradeDistributionModalComponent
    ],
    exports: [
        TooltipModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    FilterByPipe,
    ModalService,
    LocalizedAttributesPipe
  ],
  entryComponents: [
    GradesErrorComponent
  ]
})
export class GradesModule {
    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/grades.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
