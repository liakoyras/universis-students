import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {environment} from '../../environments/environment';
import {SharedModule} from '@universis/common';
import {GradesService} from './services/grades.service';

@NgModule({
  imports: [
      CommonModule,
      TranslateModule,
      SharedModule,
      MostModule
  ],
  declarations: [
  ],
    exports: [
    ],
    providers: [
      GradesService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GradesSharedModule {

    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/grades.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
