import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ProfileService } from '../../profile/services/profile.service';


export enum REGISTRATION_STATUS {
    Error = 0,
    Successful = 1,
    Available = 2,
    Postgraduate = 3,
    Deadline = 4,
    NotAvailableTill = 5,
    NotActiveStudent = 6,
    NotAvailable = 7
}

export namespace REGISTRATION_STATUS {
    export function toString(status: REGISTRATION_STATUS): string {
        return REGISTRATION_STATUS[status];
    }
}

@Injectable()
export class CurrentRegistrationService {
    // Current registration, including all the courses to be registered
    private currentRegistration: any = {};

    private ects;

    constructor(private _context: AngularDataContext, private _profileService: ProfileService) {

    }

    getLastRegistration() {
        return this._context.model('students/me/registrations')
            .asQueryable()
            .orderByDescending('academicYear')
            .thenByDescending('academicPeriod')
            .getItem();
    }

    getCurrentRegistrationEffectiveStatus() {
      return  this._context.model('students/me/currentRegistration/effectiveStatus')
        .asQueryable()
        .getItem()
        .then(registrationEffectiveStatus => {
            // save current registration to session storage
            // so it is not fetched multiple times
            registrationEffectiveStatus.initialCode = registrationEffectiveStatus.code;
            sessionStorage['registrationEffectiveStatus'] = JSON.stringify(registrationEffectiveStatus);
            return registrationEffectiveStatus;
        });
    }

    getAvailableClasses() {
      if ( sessionStorage['availableClasses']) {
        return new Promise( (resolve, reject) => {
          resolve(JSON.parse(sessionStorage['availableClasses']));
        });
      } else {
      return this._context.model('students/me/availableClasses')
        .asQueryable()
        .expand('courseType($expand=locale),' +
          'courseClass($expand=course($expand=locale),' +
          'instructors($expand=instructor($select=InstructorSummary)))')
        .take(-1)
        .getItems()
        .then(availableClasses => {
            // save available classes to session storage
            // so they are not fetched multiple times
            sessionStorage['availableClasses'] = JSON.stringify(availableClasses);
            return availableClasses;
        });
      }
    }

    getStudyLevel() {
        return this._context.model('students/me/studyProgram')
                    .select('studyLevel')
                    .getItem();
    }

    getSpecialties() {
        return this._context.model('students/me/studyProgram')
                    .asQueryable()
                    .expand('specialties')
                    .getItems();
    }

    getCurrentSpecialty() {
        return this._context.model('students/me/specialty')
                    .asQueryable()
                    .getItems();
    }

    setSpecialty(specialty) {
        // Reset available classes and effective status on specialty selection
        sessionStorage.removeItem('registrationEffectiveStatus');
        sessionStorage.removeItem('availableClasses');
        return this._context.model('students/me/specialty')
                    .save(specialty);
    }

    // To get the current registration we have 2 options during a session:
    // If user registered for some courses during the CURRENT session
    // then the current registration will be saved in session storage.
    // Else, we get the current registration by the API call.
    getCurrentRegistration() {
      if ( sessionStorage['RegistrationLatest']) {
          return new Promise( (resolve, reject) => {
              resolve(JSON.parse(sessionStorage['RegistrationLatest']));
          });
      } else {
      return this._context.model('students/me/currentRegistration')
        .asQueryable()
        .expand('classes($expand=courseType($expand=locale),courseClass($expand=course($expand=locale),' +
          'instructors($expand=instructor($select=InstructorSummary))))')
        .getItem().then(currentRegistration => {
          if (currentRegistration == null) {
            // create new registration for current year period
            return this._profileService.getStudent().then(student => {
              currentRegistration = {
                student: student.id,
                registrationYear: student.department.currentYear,
                registrationPeriod: student.department.currentPeriod,
                classes: []
              };
              sessionStorage.setItem('RegistrationLatest', JSON.stringify(currentRegistration));
              sessionStorage.setItem('InitialRegistration', JSON.stringify(currentRegistration.classes));
              return currentRegistration;
            });
          }
            // save the initial registraiton to session storage
            // in order to track changes and show/hide the submit button
            sessionStorage['InitialRegistration'] = JSON.stringify(currentRegistration.classes);
            return currentRegistration;
        }).catch(err => {
            if (err.status === 404) {
                return this._profileService.getStudent().then(student => {
                    if (student.studentStatus.alternateName !== 'active'
                    && student.studentStatus.alternateName !== 'candidate') {
                        return Promise.reject(err);
                    }
                    // create new registration for current year period
                    const newRegistration = {
                        student: student.id,
                        registrationYear: student.department.currentYear,
                        registrationPeriod: student.department.currentPeriod,
                        classes: []
                    };
                    // set newRegistration to session storage variable
                    sessionStorage.setItem('RegistrationLatest', JSON.stringify(newRegistration));
                    sessionStorage.setItem('InitialRegistration', JSON.stringify(newRegistration.classes));
                    return Promise.resolve(newRegistration);
                });
            }
            return Promise.reject(err);
        });
      }
    }

    saveCurrentRegistration() {
        return this.getCurrentRegistration().then(currentRegistration => {
          currentRegistration.classes.forEach((course) => {
            course.courseClass = course.courseClass.id;
          });
            return this._context.model('students/me/currentRegistration')
                .save(currentRegistration);
        });
    }

    // function that registers for the clicked course
    registerForCourse(course: any) {
        // Save the renewed current registration to session storage
        return this.getCurrentRegistration().then(currentReg => {
            if (currentReg.classes) {
                // push the new registered course to the current Registration array
                currentReg.classes.push(course);
            } else {
                Object.assign(currentReg, {classes: []});
                currentReg.classes.push(course);
            }
            // convert it into string in order to save it to session storage
            const tempString = JSON.stringify(currentReg);

            // save to session storage
            sessionStorage.setItem('RegistrationLatest', tempString);

            return 0;
        });
    }

    identifierCompare(a, b) {
        // tslint:disable-next-line:triple-equals
        return !!(a && b && (a == b || a.id == b || a == b.id || (a.id && b.id && a.id == b.id)));
    }

    removeCourse(course) {
        // remove from current registration
        return this.getCurrentRegistration().then(currentReg => {
            // find course index
            for (let i = 0; i < currentReg.classes.length; ++i) {
                if (this.identifierCompare(currentReg.classes[i].courseClass, course.courseClass)) {
                    // remove course from current registration
                    currentReg.classes.splice(i, 1);
                    break;
                }
            }
            // convert current registration into string in order to save it to session storage
            const tempString = JSON.stringify(currentReg);

            // save to session storage
            sessionStorage.setItem('RegistrationLatest', tempString);

            return 0;
        });
    }

    getStudentStatus() {
        return this._context.model('students/me/studentStatus')
                    .asQueryable()
                    .getItems();
    }

    reset() {
      sessionStorage.removeItem('RegistrationLatest');
      sessionStorage.removeItem('InitialRegistration');
      sessionStorage.removeItem('edit');
      sessionStorage.removeItem('registrationEffectiveStatus');
      sessionStorage.removeItem('registrationEdited');
    }

    registerSemester() {
        return this._context.model('students/me/currentRegisterAction').save({});
    }

    async getCurrentRegisterAction() {
        try {
            return await this._context.model('students/me/currentRegisterAction').asQueryable().getItem();
        } catch (err) {
            if (err.status === 404) {
                return null;
            }
            throw err;
        }
    }


    getRegistrationStatus(): Promise<any> {
        const self = this;
        // get student
        return new Promise<number> ((resolv, reject) => {
            this._profileService.getStudent().then(student => {
                // get current registration
                this.getCurrentRegistrationEffectiveStatus().then(currentStatus => {
                    this.getStudentStatus().then(studentStatus => {
                        if (!student) {
                            return reject(new Error('Current student cannot be found'));
                        }
                        if ( !studentStatus || !currentStatus ) {
                            // Undefined studentStatus || currentStatus
                            return reject(0);
                        }
                        if ( studentStatus.alternateName !== 'active'
                            && studentStatus.alternateName !== 'candidate') {return resolv(REGISTRATION_STATUS.NotActiveStudent); }

                        // Undergraduate
                        if (student.studyProgram.studyLevel === 1) {
                            if (currentStatus.status === 'open') {
                                if (student.department.isRegistrationPeriod) {
                                    // Available
                                    if (currentStatus.code === 'OPEN_NO_TRANSACTION') {
                                        return resolv(REGISTRATION_STATUS.Available);
                                    } else {
                                        // Already Registered
                                        return resolv(REGISTRATION_STATUS.Successful);
                                    }
                                }
                            } else {
                                // Deadlined passed
                                if (currentStatus.code === 'CLOSED_NO_REGISTRATION'
                                    || currentStatus.code === 'CLOSED_REGISTRATION_PERIOD') {
                                    return resolv(REGISTRATION_STATUS.Deadline);
                                } else {
                                    // NotAvailableTill
                                    return resolv(REGISTRATION_STATUS.NotAvailableTill);
                                }
                            }
                        // Postgraduate
                        } else if (student.studyProgram.studyLevel === 2) {
                            return resolv(REGISTRATION_STATUS.Postgraduate);
                        }
                    });
                });
            });
        });
    }
    async getClassSections (courseClass: any) {
      try {
      return await this._context.model(`courseClasses/${courseClass}/sections?analytics=true`)
        .asQueryable()
        .getItems();
        } catch (err) {
        if (err.status === 404) {
          console.log(err);
        } else {
          throw err;
        }
    }
  }

    /**
     *
     * Fetches the available program groups for the current student
     *
     */
    getAvailableProgramGroups() {
      if (sessionStorage['availableProgramGroups']) {
        return new Promise((resolve, reject) => {
          resolve(JSON.parse(sessionStorage['availableProgramGroups']));
        });
      } else {
        return this._context.model('students/me/availableProgramGroups')
          .asQueryable()
          .getItems()
          .then(availableProgramGroups => {
            // save available program groups to session storage
            // so they are not fetched multiple times
            sessionStorage['availableProgramGroups'] = JSON.stringify(availableProgramGroups);
            return availableProgramGroups;
          });
      }
    }

    /**
     *
     * Decides whether the student has changed their registration
     *
     */
    isRegistrationChanged() {
      return !!sessionStorage['RegistrationLatest'];
    }
}
